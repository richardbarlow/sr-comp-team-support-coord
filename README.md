# Student Robotics Competition Team Support Coordinator

This repo contains files relevant to the Competition Team Support Coordinator's role. The documentation for the Competition Team Support Coordinator can be found in the associated wiki on BitBucket (https://bitbucket.org/richardbarlow/sr-comp-team-support-coord/wiki/).
